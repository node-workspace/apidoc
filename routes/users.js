var express = require('express');
var router = express.Router();

/**
 * @api {get} /users List all users
 * @apiGroup users
 * @apiSuccess {Object[]} users User's list
 * @apiSuccess {Number} users.id User id
 * @apiSuccess {String} users.firstName User firstName
 * @apiSuccess {String} users.lastName User lastName
 * @apiSuccess {Date} users.modifiedOn Update's date
 * @apiSuccess {Date} users.createdOn Register's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    [{
 *      "id": 1,
 *      "firstName": "Monish",
 *      "lastName": "Tarai"
 *      "modifiedOn": "2018-02-10T15:46:51.778Z",
 *      "createdOn": "2018-02-10T15:46:51.778Z"
 *    }]
 * @apiErrorExample {json} List error
 *    HTTP/1.1 500 Internal Server Error
 */
router.get('/', function (req, res, next) {
  res.json([{
    "id": 1,
    "firstName": "Monish",
    "lastName": "Tarai",
    "modifiedOn": "2018-02-10T15:46:51.778Z",
    "createdOn": "2018-02-10T15:46:51.778Z"
  }]);
});

/**
 * @api {get} /users/:id Find a user
 * @apiGroup users
 * @apiParam {id} id User id
 * @apiSuccess {Number} id User id
 * @apiSuccess {String} firstName User firstName
 * @apiSuccess {String} lastName User lastName?
 * @apiSuccess {Date} modifiedOn Update's date
 * @apiSuccess {Date} createdOn Register's date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      "id": 1,
 *      "firstName": "Monish",
 *      "lastName": "Tarai"
 *      "modifiedOn": "2018-02-10T15:46:51.778Z",
 *      "createdOn": "2018-02-10T15:46:51.778Z"
 *    }
 * @apiErrorExample {json} User not found
 *    HTTP/1.1 404 Not Found
 * @apiErrorExample {json} Find error
 *    HTTP/1.1 500 Internal Server Error
 */
router.get('/:id', function (req, res, next) {
  res.json({});
});

/**
 * @api {post} /users Register a new user
 * @apiGroup users
 * @apiParam {String} firstName User firstName
 * @apiParam {String} lastName User lastName
 * @apiParamExample {json} Input
 *    {
 *      "firstName": "Monish",
 *      "lastName": "Tarai"
 *    }
 * @apiSuccess {Number} id User id
 * @apiSuccess {String} firstName User firstName
 * @apiSuccess {String} lastName User lastName
 * @apiSuccess {Date} modifiedOn Update date
 * @apiSuccess {Date} createdOn Register date
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
 *      "id": 1,
 *      "firstName": "Monish",
 *      "lastName": "Tarai",
 *      "modifiedOn": "2018-02-10T15:46:51.778Z",
 *      "createdOn": "2018-02-10T15:46:51.778Z"
 *    }
 * @apiErrorExample {json} Register error
 *    HTTP/1.1 500 Internal Server Error
 */
router.post('/', function (req, res, next) {
  // business logic for update a task
});

/**
 * @api {put} /users/:id Update a use
 * @apiGroup users
 * @apiParam {id} id User id
 * @apiParam {String} firstName User firstName
 * @apiParam {String} lastName User lastName
 * @apiParamExample {json} Input
 *    {
 *      "firstName": "Monish Kumar",
 *      "lastName": "Tarai"
 *    }
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 204 No Content
 * @apiErrorExample {json} Update error
 *    HTTP/1.1 500 Internal Server Error
 */
router.put('/:id', function (req, res) {
    // business logic for update a user
});

/**
* @api {delete} /users/:id Remove a user
* @apiGroup users
* @apiParam {id} id User id
* @apiSuccessExample {json} Success
*    HTTP/1.1 204 No Content
* @apiErrorExample {json} Delete error
*    HTTP/1.1 500 Internal Server Error
*/
router.delete('/:id', function(req, res) {
  // business logic for deleting a user
});

module.exports = router;
